﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;


public class Letter_blink : MonoBehaviour
{
    [SerializeField]private Color   color_blink;
    private InputField  m_inputfield;
    private Color       c_color_def;


    private void Start()
    {
        m_inputfield    = GetComponent<InputField>();
        if (m_inputfield != null)
            c_color_def     = m_inputfield.placeholder.color;
    }


    public void Play()
    {
        if (m_inputfield != null)
        {
            StopCoroutine("iBlink");
            StartCoroutine("iBlink");
        }
    }


    IEnumerator iBlink()
    {
        for (int i = 0; i < 3; i++)
        {
		    for ( float t = 0.0f; t < 1.0f; t += Time.deltaTime / 0.15f)
		    {
			    Color _color = ( (i/2) == 0 ) ? Color.Lerp( c_color_def, color_blink, t) : Color.Lerp( color_blink, c_color_def, t );
			    m_inputfield.placeholder.color = _color;
			    yield return null;
		    }
        }

        m_inputfield.placeholder.color = c_color_def;
    }
}
