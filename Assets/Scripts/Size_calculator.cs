﻿using UnityEngine;
using UnityEngine.UI;


public class Size_calculator 
{
    private RectTransform   m_rectParent;
    private Font            m_font;

    private float f_letter_w;
    private float f_letter_h;
    private float f_letter_space_w;
    private float f_letter_space_h;


    public Size_calculator( RectTransform _rectTr, Font _font, float _letter_fill, int _w_Count, int _h_Count )
    {
        m_rectParent    = _rectTr;
        m_font          = _font;

        f_letter_w  = (int)(m_rectParent.rect.width / _w_Count) * _letter_fill;
        f_letter_h  = (int)(m_rectParent.rect.height / _h_Count) * _letter_fill;

        f_letter_space_w = (int)(m_rectParent.rect.width - (f_letter_w * _w_Count ) ) / ( _w_Count + 1 );
        f_letter_space_h = (int)(m_rectParent.rect.height - (f_letter_h * _h_Count ) ) / ( _h_Count + 1 );
    }


    public RectTransform CreateLetter( int _w, int _h, string _str )
    {
        GameObject _letter = new GameObject("letter_"+_str);
        _letter.transform.SetParent( m_rectParent.transform );
        RectTransform _rectTr = _letter.AddComponent<RectTransform>();
        _rectTr.anchorMin = new Vector2(0, 0);
        _rectTr.anchorMax = new Vector2(1, 1);
        _rectTr.pivot = new Vector2(0.5f, 0.5f);

        float _x = f_letter_space_w + (f_letter_space_w * _w) + ( f_letter_w * _w );
        float _y = f_letter_space_h + (f_letter_space_h * _h) + ( f_letter_h * _h );

        _rectTr.offsetMin = new Vector2( _x, (m_rectParent.rect.height - _y - f_letter_h ) );
        _rectTr.offsetMax = new Vector2( -(m_rectParent.rect.width -_x - f_letter_w),  - _y );

        _letter.AddComponent<CanvasRenderer>();
        Text _txt   = _letter.AddComponent<Text>();
        _txt.font   = m_font;
        _txt.text   = _str;
        _txt.alignment = TextAnchor.MiddleCenter;
        _txt.fontSize = (int)( Mathf.Min( f_letter_w, f_letter_h) * 0.8f );

        return _rectTr;
    }

}
