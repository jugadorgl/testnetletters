﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Letter_controller : MonoBehaviour
{
    [SerializeField]private RectTransform   m_Borders;
    [SerializeField]private InputField      m_Width;
    [SerializeField]private InputField      m_Height;
    [SerializeField]private Slider          m_Fill_slider;
    [SerializeField]private Font            m_Font;

    [SerializeField][Range(0.1f, 0.9f)]	private	float	f_letter_fill		= 0.5f;

    private string[]    m_alfabet   = new string[] { "A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"};

    private List<RectTransform>  m_letters   = new List<RectTransform>();
    private Size_calculator m_sizeCalc;
    private int         _w      = 0;
    private int         _h      = 0;


    private void ClearLetters()
    {
        for ( int i = (m_Borders.childCount-1);  i >= 0; i-- )
        {
            Destroy( m_Borders.GetChild(i).gameObject );
        }
        m_letters.Clear();
    }


    private void ButtonsEnable( bool value )
    {
        Button[] _buttons = GetComponentsInChildren<Button>();
        foreach( Button _b in _buttons)
            _b.interactable = value;
    }


    public void Generate()
    {
        ClearLetters();

        if ( m_Width.text != "" && m_Height.text != "")
        {
            _w = System.Convert.ToInt32(m_Width.text);
            _h = System.Convert.ToInt32(m_Height.text);

            m_sizeCalc = new Size_calculator( m_Borders, m_Font, f_letter_fill, _w, _h );
            for ( int x = 0;  x < _w; x++ )
                for ( int y = 0;  y < _h; y++ )
                {
                    int _rnd = Random.Range(0, (m_alfabet.Length-1) );
                    m_letters.Add ( m_sizeCalc.CreateLetter( x, y, m_alfabet[ _rnd ] ) );
                }

        }
        else
        {
            if ( m_Width.text == "" )
                m_Width.GetComponent<Letter_blink>()?.Play();
            if ( m_Height.text == "" )
                m_Height.GetComponent<Letter_blink>()?.Play();
        }

    }

    
    public void FillLetter()
    {
        f_letter_fill   = m_Fill_slider.value;
    }


    public void Shuffle()
    {
        if ( m_letters.Count > 1 )
        {
            StartCoroutine( iShuffle() );
        }
    }


    IEnumerator iShuffle()
    {
        ButtonsEnable(false);

        List<CPos>  m_positions = new Letter_shuffle_Positions( m_letters ).m_rect_positions;

		for ( float t = 0.0f; t < 1.0f; t += Time.deltaTime / 2.0f)
		{
            foreach( CPos _pos in m_positions)
            {
                _pos._rectTr.offsetMin = Vector2.Lerp( _pos._offsetMin1, _pos._offsetMin2, t );
                _pos._rectTr.offsetMax = Vector2.Lerp( _pos._offsetMax1, _pos._offsetMax2, t );
            }
			yield return null;
		}

        foreach( CPos _pos in m_positions)
        {
            _pos._rectTr.offsetMin = _pos._offsetMin2;
            _pos._rectTr.offsetMax = _pos._offsetMax2;
        }

        ButtonsEnable(true);
    }
}
