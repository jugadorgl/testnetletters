﻿using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class CPos
{
    public  RectTransform   _rectTr;
    public  Vector2  _offsetMin1;
    public  Vector2  _offsetMax1;

    public  Vector2  _offsetMin2;
    public  Vector2  _offsetMax2;

    public CPos ( RectTransform _rect1, RectTransform _rect2 )
    {
        _rectTr     = _rect1;
        _offsetMin1 = _rect1.offsetMin;
        _offsetMax1 = _rect1.offsetMax;
        _offsetMin2 = _rect2.offsetMin;
        _offsetMax2 = _rect2.offsetMax;
    }
}


public class Letter_shuffle_Positions
{
    public List<CPos>  m_rect_positions = new List<CPos>();

    // Randomize
    public Letter_shuffle_Positions ( List<RectTransform> _rect )
    {
        List<RectTransform> _rect1 = new List<RectTransform>();
        List<RectTransform> _rect2 = new List<RectTransform>();

        for ( int i = 0; i < _rect.Count; i++)
        {
            _rect1.Add ( _rect[i] );
            _rect2.Add ( _rect[i] );
        }

        m_rect_positions.Clear();
        for ( int i = 0; i < _rect.Count; i++)
        {
            int _counter = 0;

            int _rnd1 = Mathf.RoundToInt( Random.value * (float)(_rect1.Count - 1) );
            int _rnd2 = Mathf.RoundToInt( Random.value * (float)(_rect2.Count - 1) );
            while ( _rect1[ _rnd1 ] == _rect2[ _rnd2 ] && _counter < 5)
            {
                _counter++;
                _rnd1 = Mathf.RoundToInt( Random.value * (float)(_rect1.Count - 1) );
                _rnd2 = Mathf.RoundToInt( Random.value * (float)(_rect2.Count - 1) );
            }

            m_rect_positions.Add( new CPos( _rect1[ _rnd1 ], _rect2[ _rnd2 ] ) );
            _rect1.RemoveAt( _rnd1 );
            _rect2.RemoveAt( _rnd2 );
        }
    }

}
